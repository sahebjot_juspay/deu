const exec = require('child_process').exec

exec('git rev-parse HEAD', (err, stdout) => {
  const gitLatestHash = stdout.substring(0, stdout.length-1)
  const installUrl = `https://bitbucket.org/sahebjot_juspay/deu/raw/${gitLatestHash}/dist/deu-mac`
  console.log(`curl -# ${installUrl} -o /usr/local/bin/deu && chmod a+x /usr/local/bin/deu`)
})

