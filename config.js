const { homedir } = require('os')
let home = homedir()

module.exports = {
  'deuPath': `${home}/.deu`,
  'cloneName': 'mystique-clone',
  'mystiqueUrl': 'bitbucket.org/juspay/mystique-boiler-plate.git',
  'deuZipUrl': 'bitbucket.org/sahebjot_juspay/deu-zip.git',
  'deuBranch': 'design/deu',
  'deuZipBranch': 'master',
  'defaultUserConf': {
    'bUsername':'',
    'currentProjects': {},
    'editor': {}
  }
}
