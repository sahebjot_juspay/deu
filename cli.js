const fs = require('mz/fs')
const chalk = require('chalk')
const U = require('./Utils')
const Haikunator = require('haikunator')
const haikunator = new Haikunator()
const app = require('./app')
const userConf = require('./userConfManager.js')
const list = require('./list')
const pkg = require('./package.json')
const editor = require('./editor')

const config = require('./config')
const cwd = process.cwd()

const commands = new Set([
    'login',
    'createApp',
    'list',
    'ls',
    'start',
    'delete',
//    'createAppFast',
    'version',
    'open',
    'openFolder',
    'ca',
    'caf',
    'rm'
])

const aliases = new Map([
  ['ls', 'list'],
  ['ca', 'createApp'],
//  ['caf', 'createAppFast'],
  ['rm', 'delete']
])

const description = new Map([
    ['createApp', 'deu createApp <appName>, appName is optional'],
    ['','']
])

const args = process.argv.slice(2)
const index = args.findIndex(a => commands.has(a))



const init = async () => {
// check if deu folder exists
// if not create it and store the settings json
// read the settings if already there
  if(await fs.exists(config.deuPath) ) {
  } else {
    U.issueHeading('Initializing...')
    await fs.mkdir(config.deuPath)
    await userConf.createUserConf(config.defaultUserConf)
  }
  await userConf.handleBitbucketUsername()
  await userConf.handleDefaultEditor()
  // check supported editors and store a default value
}

let cmd = ''

const exitIfNotPresent = (cmd, valueHasToExist) => {
  if(!valueHasToExist){
    U.issueError(`invalid arguments for '${cmd}' command`)
    process.exit(0)
  }
  return valueHasToExist
}

// turn each command to function calls
const runCommand = async (cmd, args) => {
  let appName = ''
  let appPath = ''
  switch(cmd) {
    case 'createApp':
      await app.createApp(cmd, args)
      break;
    case 'list':
      if(!args[0])
        await list.listProjects()
      if(args[0] =='editors') { // separate switch case in command.js=
        await list.listEditors()
      }
      // list something
      process.exit(0)
      break;
    case "start":
      appName = exitIfNotPresent(cmd, args[0])
      appPath = await U.getAppPath(appName)
      // check if app dependencies were installed
      await app.startAppAndOpenInEditor(appName, appPath)
      break;
    case 'delete':
      appName = exitIfNotPresent(cmd, args[0])
      appPath = await U.getAppPath(appName)
      await(app.deleteApp(appName, appPath))
      process.exit(0)
      break;
    case "open":

      break;
    case 'version':
      U.issueHeading(pkg.version)
      process.exit(0)
      break;
    default:
      U.issueError(cmd + ' command not handled yet') // find why stuck here
      break;
  }
}

// start
try {
  (async function(){
    await(init()) // check and handle initial conditions

    if (index != -1) {
      cmd = args[index]
      cmd = aliases.has(cmd)? aliases.get(cmd): cmd
      args.shift()
      await(runCommand(cmd, args))
    } else {
      if(cmd) cmd += ''
      U.issueError(cmd + 'command does not exist')
      process.exit(0)
    }

  })()
} catch (e) {
  U.issueError(e.message)
  process.exit(0)
}
