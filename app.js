const exec = require('mz/child_process').exec
const normalExec = require('child_process').exec
const config = require('./config')
const U = require('./Utils')
const fs = require('mz/fs')
const userConf = require('./userConfManager')
const rimraf = require('rimraf')
const Haikunator = require('haikunator')
const haikunator = new Haikunator()
const editor = require('./editor')
const cwd = process.cwd()

const cloneApp = async (appName, appPath, cloneUrl, branch) => {
  // check if the app name is not already existing in the path
  // U.updateText(`creating app at ${appPath}`)
  // only clone the particular branch
  return await(exec(`git clone -b ${branch} --single-branch ${cloneUrl} ${appPath}`))
}

const cloneDeuBranch = async (appName, appPath) => {
  await exec(`git config --system core.askpass git-gui--askpass`)
  let userConfig = await(userConf.getConfig())
  const cloneUrl = `https://${userConfig.bUsername}@${config.mystiqueUrl}`
  await cloneApp(appName, appPath, cloneUrl, config.deuBranch)
//  await(exec(`git checkout ${config.deuBranch}`, {cwd: appPath}))
	// unset the git gui thing
	await exec(`git config --system --unset-all core.askpass`)
  return await(userConf.storeProject(appName, appPath))
}


const installDeps = (appPath) => {
  U.issueHeading('installing app dependencies')
  return exec('npm i', {cwd: appPath})
}

const startApp = async (appName, appPath) => {
  U.issueHeading('starting application...')
  let userConfig = await(userConf.getConfig())
  let stdout = ''
  let stderr = ''
	let processed = false
  return new Promise((resolve, reject) => {
    let npmStart = normalExec(`npm start --id=${userConfig.bUsername}`, {cwd: appPath})
    npmStart.stdout.on('data', (data) => {
      stdout += data
      if(stdout.includes('webpack: Compiled successfully') && !processed){
				processed = true
        U.issueHeading(`${appName} ready to use`)
        return resolve(stdout)
      }
    });
    npmStart.stderr.on('data', (data) => {
      stderr += data
        if(stderr.includes('EADDRINUSE')) {
          U.throwErrorAndQuit(`Another app is already running or the port is in use`)
        }
    })

  })
}

exports.deleteApp = async (appName, appPath) => {
  U.issueHeading(`Deleting app ${appName}`)
  return new Promise((resolve, reject) => {
    rimraf(appPath, {}, async (err) => {
      if(err) return reject(err)
      await userConf.deleteProject(appName)
      return resolve(appName)
    })
  })
}

exports.cloneAppZipBranch = async (appName, appPath) => {
  let userConfig = await(userConf.getConfig())
  const cloneUrl = `https://${userConfig.bUsername}@${config.deuZipUrl}`
  await cloneApp(appName, appPath, cloneUrl, config.deuZipBranch)
//  await exec(`git checkout ${config.deuZipBranch}`, {cwd: appPath})
  return await(userConf.storeProject(appName, appPath))
}

const copyMystiqueAsApp = async (appName, appPath, ifNotFirstApp) => {
  if(ifNotFirstApp) {
    await userConf.storeProject(appName, appPath)
  }
  return await exec(`cp -r ${config.deuPath}/${config.cloneName} ${appPath}`)
}

exports.createApp = async (cmd, args) => {
      let appName = args[0] || haikunator.haikunate({tokenLength: 0, delimiter: ''})
      let appPath = cwd + "/" + appName
      let clonePath = `${config.deuPath}/${config.cloneName}`
      let ifNotFirstApp = await fs.exists(clonePath)
      U.issueHeading('creating app ' + appName)
      if(ifNotFirstApp) {
      } else {
        await cloneDeuBranch(appName, clonePath)
        await installDeps(clonePath) // after this store flag that dependencies were installed
      }
      await copyMystiqueAsApp(appName, appPath, ifNotFirstApp)
      await exec(`chown -R $USER ${appPath}`)
      await startApp(appName, appPath)
      if(await editor.ifAnyEditorSupported()) {
        U.issueHeading('opening in defaultEditor')
        await editor.openInDefaultEditor(appPath)
      }
}

exports.startAppAndOpenInEditor = async (appName, appPath) => {
  await startApp(appName, appPath)

  if(await editor.ifAnyEditorSupported()) {
    U.issueHeading('opening in defaultEditor')
    await editor.openInDefaultEditor(appPath)
  }
}


// maybe make a watch app build

exports.copyMystiqueAsApp = copyMystiqueAsApp
exports.startApp = startApp
exports.installDeps = installDeps
exports.cloneDeuBranch = cloneDeuBranch
