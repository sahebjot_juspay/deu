const chalk = require('chalk')
const log = console.log
const readline = require('readline')
const userConf = require('./userConfManager')
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

const issueError = (errorLine) => {
  log(chalk.red(errorLine))
}

exports.askQuestion = (question) => {
  return new Promise((resolve, reject) => {
    rl.question(chalk.blue(question), (answer) => {
      rl.close()
      return resolve(answer)
    })
  })
}

exports.issueHeading = (headingText) => {
  log(chalk.green.bold(headingText))
}

exports.updateText = (updateText) => {
  log(chalk.dim(updateText))
}

exports.listItems = (itemArray) => {
  log(chalk.magenta(itemArray.join("\n")))
}

exports.getAppPath = async (appName) => {
  let userConfig = await userConf.getConfig()
  let app = userConfig.currentProjects[appName]
  return app?app.path:throwErrorAndQuit(`${appName} does not exist`)

}

const throwErrorAndQuit = (errMessage) => {
  issueError(errMessage)
  process.exit(0)
}
exports.issueError = issueError
exports.throwErrorAndQuit = throwErrorAndQuit
