module.exports = {
	context: __dirname,
	entry: "./cli",
	output: {
		path: __dirname + "/dist",
		filename: "bundle.js",
	},
  target: 'node'
}
