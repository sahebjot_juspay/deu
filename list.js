const userConf = require('./userConfManager')
const U = require('./Utils')
const editor = require('./editor')
const chalk = require('chalk')

const listProjects = async () => {
  let userConfig = await userConf.getConfig()
  let projects = Object.keys(userConfig.currentProjects)
  projects = projects.map((p,i) => `${i+1}.) ${p}`)
  if (!projects.length) {
    projects = [ "No apps made yet"]
  }
  return U.listItems(projects)
}

const listEditors = async () => {
  U.listItems((await editor.getSupportedEditors()).map(editorObj => {
    let supportedText = editorObj.supported? chalk.green.bold('supported'): chalk.red('not supported')
    return `${editorObj.name} ${supportedText}`
  }))
}

exports.listProjects = listProjects
exports.listEditors = listEditors
