const { type } = require('os')
const fs = require('mz/fs')
const chalk = require('chalk')
const userConf = require('./userConfManager')
const exec = require('mz/child_process').exec

if (type() === 'Darwin') {
}
const supportedEditors = {
		'Darwin': {
			'sublime': ['/Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl'],
			'atom': ['/usr/local/bin/atom', '/Applications/Atom.app/Contents/Resources/app/atom.sh']
		},
		'Linux': ''
}

const getEditors = async () => {
	let osEditors = supportedEditors[type()]
	if(osEditors) {
		return await Promise.all(Object.keys(osEditors).map(async editor => {
			let editorReport = await Promise.all(osEditors[editor].map((editorPath) => {
				return fs.exists(editorPath)
			}))
			let supportedIndex = editorReport.indexOf(true)
      let editorObj = {name: editor, supported: (supportedIndex!=-1)}
      if(supportedIndex != -1) {
        editorObj.path = osEditors[editor][supportedIndex]
      }
      return editorObj
		}))
	} else {
		return ["No supported editors detected"]
	}
}

const getSupportedEditors = async () => {
  let editors = await getEditors()
  return editors.filter(editor => editor.supported)
}

exports.setDefaultEditor = async (editor) => {
  let conf = await userConf.getConfig()
  conf.editor = editor
  return userConf.writeConfig(conf)
}


exports.openInDefaultEditor = async (appPath) => {
  let editor = await getDefaultEditor()
  let path = editor.path
  return exec(`'${path}' ${appPath}`)
}

const getDefaultEditor = async () => {
  let conf = await userConf.getConfig()
  return conf.editor
}

exports.ifAnyEditorSupported = async () => {
  let supportedEditors = (await getEditors())
  return supportedEditors.map(editorObj => {
    return editorObj.supported
  }).indexOf(true) != -1
}

exports.getEditors = getEditors
exports.getSupportedEditors = getSupportedEditors
exports.getDefaultEditor = getDefaultEditor
