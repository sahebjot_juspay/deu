const fs = require('mz/fs')
const U = require('./Utils')
const config = require('./config')
const Project = require('./project')
const userConfPath = `${config.deuPath}/config.json`
const editor = require('./editor')

exports.createUserConf = (userConf) => {
  return fs.writeFile(userConfPath, JSON.stringify(userConf), 'utf8')
}

const writeConfig = (userConf) => {
  return fs.writeFile(userConfPath, JSON.stringify(userConf), 'utf8')
}

const addKeyWithValue = async (key, value) => {
  let userConf = await(getConfig())
  userConf[key] = value
  writeConfig(userConf)
}

exports.appendValueToKey = (key, value) => {

}

exports.addKeyWithValue = (key, value) => {

}

exports.replaceUserConf = (newConf) => {

}

const getConfig = async () => {
  return JSON.parse(await(fs.readFile(userConfPath, 'utf8')))
}

exports.handleBitbucketUsername = async () => {
  let userConf = await(getConfig())
    if(userConf.bUsername) {
      return
    } else {
    let bUsername = await(U.askQuestion('Enter bitbucket username '))
      if(!bUsername) return U.issueError("invalid username")
    return await(addKeyWithValue("bUsername", bUsername))
    }
}

exports.handleDefaultEditor = async () => {
    if(await editor.ifAnyEditorSupported()) {
      let supportedEditors = await editor.getSupportedEditors()
      await editor.setDefaultEditor(supportedEditors[0])
    } else {
      //U.issueError('Cound not detect an editor')
    }
}

const storeProject = async (name, path) => {
  let newProject = Project({"appName": name, "appPath": path})
  const userConf = await(getConfig())
  userConf.currentProjects[name] = newProject
  return await(writeConfig(userConf))
}

const deleteProject = async (name) => {
  const userConf = await(getConfig())
  delete userConf.currentProjects[name]
  return await writeConfig(userConf)
}

exports.getConfig = getConfig
exports.addKeyWithValue = addKeyWithValue
exports.storeProject = storeProject
exports.deleteProject = deleteProject
exports.writeConfig = writeConfig
