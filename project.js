module.exports = (opts) => {
  return {
    createdAt: new Date(),
    name: opts.appName,
    path: opts.appPath,
    lastUpdated: new Date()
  }
}
